# Chuck Norris Integration

This project consumes the [Chuck Norris Jokes API](https://api.chucknorris.io/). It is comprised of a web docker image
and a redis docker image. The web image uses node to build and serve the project.

## Getting setup
Clone the repository using
```git
git clone https://bitbucket.org/Kieranwilson/chuck-norris-integration.git
```
After this, copy the `docker/.env.sample` to `docker/.env` and run `docker-compose up`

The docker image will automatically install all the required node packages and build the project from the source files.

### Features

The node image features got module reloading while in development mode and nodemon to automatically restart node upon changes.
When in production mode, it is minifies the build files and does not use nodemon or watchify.

The build tasks are all contained inside in the package.json file along with their configuration. The exception to this is
the `.babelrc` file which is necessary for vueify as it does not check the package.json file for configurations.

### Why Node?

This is my first node project, seeming like a relatively small application that mostly depends on simple API calls, it
would have less overhead with Node and also gives me something to challenge myself with.

I am already very capable in PHP and wrote something very similar but slightly more complex in PHP very recently and
didn't want to feel like it was more or less of a "copy-paste project". That project is available
[here](https://bitbucket.org/Kieranwilson/atlas-api).

### Why Redis

Redis is perfect for very fast caching and response times while also being very easy to setup and write code for.
Redis' ordered lists are the perfect way to store and track leaderboards without additional coding

#### Future Improvements

The most obvious one would be some heavy refactoring especially on the home component and the node routes file. The routes
file should be a bit more modular and have alot of the code defined elsewhere. As part of this implementing a local variable
to the search rote that tracks the current searches in progress to prevent duplication that currently occurs.

There needs to be proper error handling across the application but specifically in the promises surrounding the routes which
would be a major issue in production. The process if moved to production should be wrapped in an auto restart ability such as
forever.

Implementing socket.io communication between the server and the clients to keep scores up to date across users and possible
notifications across the site when users vote on jokes.

Having a "link directly to this joke" so that single jokes could be bookmarked or shared.

I would like to add a simple text field for users to enter their own name which would pass this to the joke component to
be used within a computed property with regex replacement for chuck norris to be replaced with their own name.

Jokes can and do get repeated and this becomes noticeable when you filter by category. This is because it just gets a
random joke and the API doesn't feature a way to say not within x, or any kind of pagination etc. The only real workaround
I though of would be to pull all the jokes on application startup / intermittently and then never refer to the original
source throughout normal runtime. There would be some work involved to do this however.
