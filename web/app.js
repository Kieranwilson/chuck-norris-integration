'use strict';

/** Application Configuration **/
const config = require('./lib/config').default;
const debug = require('debug')(config.debug.namespace+'application');
debug('Booting application');

/** Application dependencies are loaded up first **/
/** Redis Client **/
const redisClient = require('./lib/redisClient')(config);
/** Chuck Norris Class **/
const chuckNorris = new (require('./lib/chuckNorris'))(config);

/** Check if we have the categories already, if not then grab them **/
redisClient.smembers('categories', function(err, reply) {
	debug('Seeing if we need to set categories');
	if (typeof reply !== "object" || reply.length === 0) {
		chuckNorris.getCategories().then(response => {
			let categories = JSON.parse(response);
			redisClient.sadd(['categories'].concat(categories), function(err, reply) {
				debug('Categories have been added');
			});
		});
	} else {
		debug("Categories are already set: " + reply.join(", "));
	}
});

require('./lib/server')(config, chuckNorris, redisClient);
