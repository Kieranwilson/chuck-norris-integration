module.exports = (config, chuckNorris, redisClient) => {
	const debug = require('debug')(config.debug.namespace+'server');
	const http = require('http');
	const express = require('express');
	/** Express Stuff **/
	const app = express();
	const server = http.createServer(app);
	/** Express Routes **/
	require('./routes')(config, app, express, chuckNorris, redisClient);
	/** Start the server and listen to requests **/
	server.listen(config.server.port);

	server.on('listening', function() {
		debug('Express server started on port %s', server.address().port);
	});

	return {
		http,
		express,
		app,
		server
	};
};
