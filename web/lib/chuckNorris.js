module.exports = class chuckNorris {
	constructor(config) {
		this.config = config;
		this.apiHost = config.chuckNorris.host;
		this.debug = require('debug')(config.debug.namespace+'chuck-norris');
	}

	makeRequest(url) {
		this.debug('Making request to: %s', url);
		let $this = this;
		// return new pending promise
		return new Promise((resolve, reject) => {
			// select http or https module, depending on reqested url
			const lib = url.startsWith('https') ? require('https') : require('http');
			const request = lib.get(url, (response) => {
				// handle http errors
				if (response.statusCode < 200 || response.statusCode > 299) {
					// TODO: Error handling here
					this.debug('Request failed, for now we just request try again, need to add error handling later.');
					$this.makeRequest(url);
				}
				// temporary data holder
				const body = [];
				// on every content chunk, push it to the data array
				response.on('data', (chunk) => body.push(chunk));
				// we are done, resolve promise with those joined chunks
				response.on('end', () => resolve(body.join('')));
			});
			// handle connection errors of the request
			request.on('error', (err) => {
				// TODO: Error handling here
				this.debug('Request failed, for now we just request try again, need to add error handling later.');
				$this.makeRequest(url)
			})
		})
	}

	getCategories() {
		return this.makeRequest(this.apiHost + '/jokes/categories');
	}

	getRandom() {
		return this.makeRequest(this.apiHost + '/jokes/random');
	}

	getFromCategory(category) {
		return this.makeRequest(this.apiHost + '/jokes/random?category=' + category);
	}

	getSearchResults(query) {
		return this.makeRequest(this.apiHost + '/jokes/search?query=' + query);
	}
};
