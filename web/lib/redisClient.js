module.exports = config => {
	var redis = require('redis');
	const debug = require('debug')(config.debug.namespace + 'redis-client');

	debug('Connecting to Redis at: %s:%s', config.redisConfig.host, config.redisConfig.port);

	var client = redis.createClient(config.redisConfig.port, config.redisConfig.host);

	client.getScoreAsync = function (listKey, member) {
		let $this = this;
		return new Promise(function (resolve, reject) {
			$this.multi()
				.exists(listKey)
				.zscore(listKey, member)
				.exec(function (err, replies) {
					if (err !== null) return reject(err);

					return replies[0] === null ? resolve(0) : resolve(replies[1]);
				});
		});
	};

	client.incrScoreAsync = function (listKey, member, count) {
		let $this = this;
		return new Promise(function (resolve, reject) {
			$this.zincrby(listKey, count, member, function (err, reply) {
				if (err !== null) return reject(err);

				debug('incrScoreAsync: Returned result is: ' + reply);
				resolve(JSON.parse(reply));
			});
		});
	};

	client.scoreRangeAsync = function (listKey, from, limit) {
		let $this = this;
		return new Promise(function (resolve, reject) {
			$this.zrevrange(listKey, from, limit, function (err, reply) {
				if (err !== null) return reject(err);

				resolve(reply);
			});
		});
	};

	client.getListItemAsync = function (key, index) {
		let $this = this;
		return new Promise(function (resolve, reject) {
			$this.lindex(key, index, function (err, reply) {
				if (err !== null) return reject(err);

				debug('getListItemAsync: Returned result is: ' + reply);
				resolve(JSON.parse(reply));
			});
		});
	};

	client.checkExpiredAsync = function (key) {
		let $this = this;
		return new Promise(function (resolve, reject) {
			$this.exists('alive:' + key, function (err, reply) {
				if (err !== null) return reject(err);

				if (reply === 0) {
					debug('%s key is not set in Redis', key);
					resolve(true);
				} else {
					debug('%s is still set', key);
					resolve(false);
				}
			});
		});
	};

	client.addListWithExpire = function (key, data) {
		let $this = this;
		return new Promise(function (resolve, reject) {
			let redisData = data.map(JSON.stringify);

			$this.multi()
				.lpush([key].concat(redisData))
				.set("alive:" + key, 1, 'EX', process.env.SEARCH_CACHING)
				.exec(function (err, replies) {
					debug("MULTI got " + replies.length + " replies");
					replies.forEach(function (reply, index) {
						debug("Reply " + index + ": " + reply.toString());
					});
					resolve(replies);
				});
		});
	};

	return client;
};
