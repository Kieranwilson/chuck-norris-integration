var path = require('path');

exports.default = {
	debug: {
		namespace: 'chuck-norris:'
	},
	redisConfig: {
		host: process.env.REDIS_HOST,
		port: process.env.REDIS_PORT
	},
	paths: {
		distribution: path.resolve(__dirname + '/../dist'),
		root: path.resolve(__dirname + '/..')
	},
	server: {
		port: '3000'
	},
	chuckNorris: {
		host: "https://api.chucknorris.io"
	}
};
