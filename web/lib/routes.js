module.exports = function (config, app, express, chuckNorris, redisClient) {
	const debug = require('debug')(config.debug.namespace + 'routes');

	/** Static Routes **/
	debug("Binding static routes");
	const routes = express.Router();
	routes.use(express.static(config.paths.distribution));

	routes.get('/', function (req, res) {
		debug('Root node requested');
		res.sendFile(config.paths.root + '/index.html');
	});

	/** API Routes **/
	debug("Binding API routes");
	const apiRoutes = express.Router();

	apiRoutes.get('/jokes/random', function (req, res) {
		chuckNorris.getRandom().then(joke => {
			/** We receive it as a json string, so we have to turn it back into an object before we pass it on **/
			joke = JSON.parse(joke);
			redisClient.getScoreAsync('leaderboard', joke.id).then((score) => {
				if (score === null)
					score = 0;
				joke.voteCount = score;

				res.json(joke);
			});
		});
	});

	apiRoutes.get('/joke/:joke', function (req, res) {
		res.json(req.params);
	});

	apiRoutes.get('/category/:category', function (req, res) {
		chuckNorris.getFromCategory(req.params.category).then(joke => {
			/** We receive it as a json string, so we have to turn it back into an object before we pass it on **/
			joke = JSON.parse(joke);
			redisClient.getScoreAsync('leaderboard', joke.id).then((score) => {
				if (score === null)
					score = 0;
				joke.voteCount = score;

				res.json(joke);
			});
		});
	});

	apiRoutes.get('/leaderboard/:index', function (req, res) {
		redisClient.scoreRangeAsync('leaderboard', parseInt(req.params.index), parseInt(req.params.index)).then(jokeId => {
			jokeId = jokeId.length ? jokeId[0] : null;
			if (jokeId !== null) {
				redisClient.getScoreAsync('leaderboard', jokeId).then((score) => {
					if (score === null)
						score = 0;
					return score;
				}).then(score => {
					redisClient.get('joke:' + jokeId, function (err, reply) {
						if (err !== null) return res.json(err);

						joke = JSON.parse(reply);
						joke.voteCount = score;
						res.json(joke);
					});
				});
			} else {
				res.json(null);
			}
		});
	});

	apiRoutes.get('/search/:query/:index', function (req, res) {
		debug('Search query as: %s', req._parsedOriginalUrl.path);
		redisClient.checkExpiredAsync('search:' + req.params.query).then(expired => {
			if (expired) {
				debug('Search is not yet set or expired, getting it again');

				return chuckNorris.getSearchResults(req.params.query).then(response => {
					let jokes = JSON.parse(response);
					redisClient.addListWithExpire('search:' + req.params.query, jokes.result)
				});
			}
		}).then(() => {
			redisClient.getListItemAsync('search:' + req.params.query, req.params.index).then((joke) => {
				if (typeof joke !== "undefined" && joke !== null) {
					redisClient.getScoreAsync('leaderboard', joke.id).then((score) => {
						if (score === null)
							score = 0;
						joke.voteCount = score;

						res.json(joke);
					});
				} else {
					res.json(joke);
				}
			});
		});
	});

	apiRoutes.get('/vote', function (req, res) {
		let joke = req.query.joke;

		delete joke.voteCount;
		redisClient.set('joke:' + joke.id, JSON.stringify(joke), function (err, reply) {
			if (err !== null) return res.json(err);
		});

		redisClient.incrScoreAsync('leaderboard', joke.id, 1).then((count) => {
			return res.json(count);
		});
	});

	apiRoutes.get('/categories', function (req, res) {
		redisClient.smembers('categories', function (err, reply) {
			if (typeof reply === "object" && reply.length > 0) {
				res.json(reply);
			} else {
				res.sendStatus(500);
			}
		});
	});

	/** Bind the routes to the app **/
	app.use('', routes);
	app.use(process.env.API_URL, apiRoutes);

	/** Route fallback, return the index, and let the spa determine if should 404 **/
	app.get('*', function (req, res) {
		debug('Catch all route requested by: %s', req._parsedOriginalUrl.path);
		res.sendFile(config.paths.root + '/index.html');
	});
}
	