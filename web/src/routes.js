import Home from './components/Home.vue';

export const routes = [
	{ path: '/', component: Home },
	{ path: '/joke/:id', component: Home, name: 'joke'  },
	{ path: '/leaderboard', component: Home, name: 'leaderboard'  },
	{ path: '/category/:name', component: Home,  name: 'category' },
	{ path: '/search/:query', component: Home,  name: 'search' },
	{ path: '*', redirect: '/' } // TODO: Add 404 handler here
];
