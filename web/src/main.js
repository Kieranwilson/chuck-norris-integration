/** I don't like jQuery anymore but bootstrap requires it
 * and bootstrap makes my styling life much easier **/
window.$ = window.jQuery = require('jquery');
require('../node_modules/bootstrap/scss/bootstrap.scss');
require('bootstrap/dist/js/bootstrap.bundle.js');


/** Vue Startup **/
import Vue from 'vue';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';
import App from './App.vue';
import {routes} from './routes';

Vue.use(VueResource);
Vue.use(VueRouter);

const router = new VueRouter({
	routes,
	mode: 'history'
});
Vue.http.options.root = process.env.API_HOSTNAME;


new Vue({
	el: '#app',
	router,
	render: h => h(App)
});
